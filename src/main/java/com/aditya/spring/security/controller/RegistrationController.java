package com.aditya.spring.security.controller;

import com.aditya.spring.security.models.entity.Role;
import com.aditya.spring.security.models.entity.UserModel;
import com.aditya.spring.security.models.web.Register;
import com.aditya.spring.security.service.RoleService;
import com.aditya.spring.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/registration")
public class RegistrationController {

    @Autowired
    private RoleService roleService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private UserService userService;

    @PostMapping
    public String register(@ModelAttribute("registerModel") Register model){

        System.out.println("Data selected: "+model.toString());

        UserModel userModel = new UserModel();
        userModel.setUsername(model.getEmail());
        userModel.setPassword(passwordEncoder.encode(model.getPassword()));
        userModel.setRoles(Collections.singletonList(roleService.findById(model.getUserRole())));
        userService.save(userModel);

        UserModel savedModel = userService.findByUsername(model.getEmail());
        System.out.println("User saved: "+savedModel.toString());

        return "redirect:/registration?success";
    }

    @GetMapping
    public String loadPage(Model model){
        model.addAttribute("registerModel", new Register());
        List<Role> roles = roleService.findAll();
        System.out.println("Roles in DB: ");
        roles.forEach(r-> System.out.println(r.getRole()));
        model.addAttribute("roles", roles);
        return "registrationform";
    }
}
