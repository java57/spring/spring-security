package com.aditya.spring.security.models.entity;

import javax.persistence.Entity;

@Entity
public class Role extends BaseEntity{

    private String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Role{" +
                "role='" + role + '\'' +
                '}';
    }
}
