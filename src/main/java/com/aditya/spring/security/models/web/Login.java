package com.aditya.spring.security.models.web;

import lombok.Data;

@Data
public class Login {

    private String username;
    private String password;
}
