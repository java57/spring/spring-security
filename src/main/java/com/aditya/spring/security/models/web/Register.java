package com.aditya.spring.security.models.web;

import lombok.Data;

@Data
public class Register {

    private String email;
    private String password;
    private Long userRole;
}
