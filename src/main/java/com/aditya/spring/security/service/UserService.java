package com.aditya.spring.security.service;

import com.aditya.spring.security.models.entity.Role;
import com.aditya.spring.security.models.entity.UserModel;
import com.aditya.spring.security.repository.UserRepo;
import com.aditya.spring.security.service.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService extends BaseService implements IUserService {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private RoleService roleService;

    public void save(UserModel model){

        if(model != null){
            prepareModel(model);
            userRepo.save(model);
        }
    }

    public UserModel findByUsername(String userName){
        return userRepo.findByUsername(userName);
    }

    public List<UserModel> getAllUsers(){
        List<UserModel> users = userRepo.findAll();
        for (UserModel user : users) {
            String[] arr = new String[user.getRoles().size()];
            int i=0;
            for (Role role : user.getRoles()) {
                arr[i] = role.getRole();
                i++;
            }
            user.setRolesString(String.join(",", arr));
        }
        return users;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserModel user = userRepo.findByUsername(username);
        if(user==null){
            System.out.println("User "+username+" not found in DB");
            throw new UsernameNotFoundException("Invalid username or password");
        }
        System.out.println("Found user in DB: "+user.getUsername());
        System.out.println("Roles size: "+user.getRoles().size());
        user.getRoles().forEach(r->{
            System.out.println(r.getRole());
        });
        return new User(user.getUsername(), user.getPassword(), mapRolesToAuthorities(user.getRoles()));
    }

    private Collection < ? extends GrantedAuthority > mapRolesToAuthorities(Collection <Role> roles) {
        System.out.println("Available roles: "+roles.stream().map(role->new SimpleGrantedAuthority(role.getRole())).collect(Collectors.toList()));
        return roles.stream().map(role->new SimpleGrantedAuthority(role.getRole())).collect(Collectors.toList());
    }
}
