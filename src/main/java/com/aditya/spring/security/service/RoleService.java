package com.aditya.spring.security.service;

import com.aditya.spring.security.models.entity.Role;
import com.aditya.spring.security.repository.RoleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class RoleService extends BaseService{

    @Autowired
    private RoleRepo roleRepo;

    @PostConstruct
    private void init(){

        Role role = new Role();
        role.setRole("ADMIN");
        save(role);

        role = new Role();
        role.setRole("USER");
        save(role);

    }

    public void save(Role model){
        if(model!=null){
            prepareModel(model);
            roleRepo.save(model);
        }
    }

    public List<Role> findAll(){
        return roleRepo.findAll();
    }

    public Role findById(Long id){
        return roleRepo.findById(id).get();
    }
}
