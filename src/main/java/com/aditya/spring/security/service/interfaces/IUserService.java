package com.aditya.spring.security.service.interfaces;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface IUserService extends UserDetailsService {

}
