package com.aditya.spring.security.service;

import com.aditya.spring.security.models.entity.BaseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class BaseService {

    protected void prepareModel(BaseEntity model){
        if (model==null){
            model.setCreatedDate(new Date());
        }
        model.setLastUpdatedDate(new Date());
    }
}
